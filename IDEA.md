# IDEAs

## Templating

Templating of .gmi files (e.g. insert %%TOC%% to Gemtext files as well). Could also template common .gmi page headers and footers. Could also insert bash code here.

## Also generate a PDF book

I could use pandoc for this (convert from Markdown to PDF). This works on Fedora Linux 34:

```
sudo dnf install pandoc wkhtmltopdf
pandoc **/*.md --pdf-engine=wkhtmltopdf --output foo.zone.pdf
```

There will be some more scripting required to get the page order and ToC correct.

## More ideas

* Automatic ToC generation.
* Sitemap generation.
* More output formats. Gopher? Groff? Plain text? PDF via Pandoc? .sh with interactive menus?
